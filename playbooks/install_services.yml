
# This playbook sets up a Kanboard instance on either a single host or on a multi-tiered deployment.
#
#   ansible-playbook -i environment/hosts.yml playbooks/site.yml
#
---
- name: Install Services
  hosts: all
  become: yes
  vars_files:
    - ../environment/group_vars/all/all.yml
  vars:
    docker__pip_packages:
      - name: "systemd-python"
    certbot_auto_renew_options: "--quiet --no-self-upgrade --pre-hook='docker stop proxy' --post-hook='docker exec -it portal \"/app/bin/run_compositional_role.sh\"'"
    certbot_admin_email: "{{ environment_admin }}@{{ environment_domain }}"
    certbot_create_if_missing: True
    certbot_install_from_source: False
    certbot_create_standalone_stop_services:
      - 'docker'
    certbot_certs:
      - domains: "{{ [environment_domain] + compositional_domain_redirects | default({}) | map(attribute='domain') | list }}"
  pre_tasks:
    - name: Update all packages on the system
      apt:
        name: "*"
        state: latest
        update_cache: True
      register: pkg_install
      retries: 60
      delay: 5
      # Here we're also saying to retry on if we're facing a 'could not get lock'.
      # See: https://docs.ansible.com/ansible/latest/collections/ansible/builtin/apt_module.html#examples
      until: pkg_install is success or (
        not pkg_install['msg'] | d() | regex_search('Failed to lock apt|Could not get lock'))

    - name: Install additional packages
      apt:
        name: "{{ pkgs }}"
        state: latest
        update_cache: True
      vars:
        pkgs:
          - "pkg-config"
          - "libsystemd-dev"
      register: pkg_install
      retries: 60
      delay: 5
      until: pkg_install is success or (
        not pkg_install['msg'] | d() | regex_search('Failed to lock apt|Could not get lock'))

    #
    # TODO: LEGACY
    #
    - name: Determine presence of legacy python-docker
      stat:
        path: '/usr/local/bin/python-docker'
      register: install_services_python_docker

    - name: Link the python version for legacy clients
      file:
        src: '/usr/local/bin/python-docker'
        path: '/usr/local/bin/python3-docker'
        state: link
      when: install_services_python_docker['exists'] is defined

  roles:
    - role: swapfile
      swapfile_size: '4G'
    - role: docker
      ansible_python_interpreter: "/usr/bin/python3"
    - role: certbot
    - role: compositionalenterprises.ourcompose.compositional
      tags: ['comp']
      ansible_python_interpreter: "/usr/bin/env python3-docker"

  post_tasks:
    - name: Create podcast download-tracking script on ourcompose.com
      block:
        - name: Create episode_stats directory
          file:
            path: "/srv/local/nginx_episode_stats"
            state: directory

        - name: Place the script
          copy:
            src: ../files/episode_stats.sh
            dest: /root/episode_stats.sh
            mode: 0755

        - name: Add cronjob to run at the top of every hour
          cron:
            name: "Run the episode stats report"
            special_time: "hourly"
            job: /root/episode_stats.sh

      when: "'{{ environment_domain }}' == 'ourcompose.com'"
      tags: ['comp']
