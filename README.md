# Compositional Play

Installs my cloud-in-a-box solution that deploys docker on a host and then sets up docker-compose to be able to install and maintain and backup various docker containers that run services for that domain.

## Invocation

```shell
# git clone <environment repo> ./environment
# ansible-galaxy install -r requirements.yml
# ansible-playbook playbooks/site.yml
```
